DocumentFragment.querySelector('#buy').addEventListener("click",()=> {
    toastr.remove()
    const car = {
        brand:document.querySelector('#brand').value,
hp:document.querySelector('#hp').value,
culoare:document.querySelector('#color').value,
capacity:document.querySelector('#capacity').value
    }
    let validSend = true;
    if(!car.brand||!car.culoare||!car.hp||!car.capacity){
        toastr.error('Unul sau mai multe campuri sunt goale')
        validSend =  false;

    }
   if(validSend == true){
       axios.post('/masina',{
           brand:car.brand,
           hp:car.hp,
           culoare:car.culoare,
           capacity:car.capacity
       })
       .then((response)=> {
           console.log("Succes!")
       })
       .catch((error) =>{
           const values= Object.values(error.response.data)
           values.map(item => {
               toastr.error(item)
           })
       })
   }

}, false) 